<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>登録フォーム | sample</title>
<?php require_once('./include/head.php'); ?>

</head>
<body>
<div id="wrapper">
	<?php require_once('./include/header.php'); ?>
	
	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="pageTitle">登録フォーム</h2>
				</div>
			</div>
		</div>
	</section>
	
	<section id="content">
		<div class="container">
			<div class="row form-container">
				
				<div class="row bs-wizard">
					<div class="col-xs-4 bs-wizard-step complete">
						<div class="text-center bs-wizard-stepnum">登録</div>
						<div class="progress"><div class="progress-bar"></div></div>
						<a href="#" class="bs-wizard-dot"></a>
					</div>
					<div class="col-xs-4 bs-wizard-step complete">
						<div class="text-center bs-wizard-stepnum">パスワード入力</div>
						<div class="progress"><div class="progress-bar"></div></div>
						<a href="#" class="bs-wizard-dot"></a>
					</div>
					<div class="col-xs-4 bs-wizard-step active">
						<div class="text-center bs-wizard-stepnum">登録完了</div>
						<div class="progress"><div class="progress-bar"></div></div>
						<a href="#" class="bs-wizard-dot"></a>
					</div>
				</div>
				
				<div class="row text-center">
					<h3>会員登録が完了いたしました。</h3>
					<p class="text-lead">
						ご登録ありがとうございます。<br>
						ご登録いただいたメールアドレス宛に詳細情報画面のURLを送信致しました。
					</p>
				</div>
				<div class="text-center submit-area">
					<a href="index.php" class="btn btn-primary btn-lg">戻る</a>
				</div>
				
			</div>
		</div>
		
	</section>
	
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>


<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Vendor Scripts -->
<script src="js/modernizr.custom.js"></script>
<script src="js/application.js"></script>

</body>
</html>