<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>登録フォーム | sample</title>
<?php require_once('./include/head.php'); ?>

</head>
<body>
<div id="wrapper">
	<?php require_once('./include/header.php'); ?>
	
	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="pageTitle">登録フォーム</h2>
				</div>
			</div>
		</div>
	</section>
	
	<section id="content">
		<div class="container">
			<div class="row form-container">
				
				<div class="row bs-wizard">
					<div class="col-xs-4 bs-wizard-step complete">
						<div class="text-center bs-wizard-stepnum">登録</div>
						<div class="progress"><div class="progress-bar"></div></div>
						<a href="#" class="bs-wizard-dot"></a>
					</div>
					<div class="col-xs-4 bs-wizard-step active">
						<div class="text-center bs-wizard-stepnum">パスワード入力</div>
						<div class="progress"><div class="progress-bar"></div></div>
						<a href="#" class="bs-wizard-dot"></a>
					</div>
					<div class="col-xs-4 bs-wizard-step disabled">
						<div class="text-center bs-wizard-stepnum">登録完了</div>
						<div class="progress"><div class="progress-bar"></div></div>
						<a href="#" class="bs-wizard-dot"></a>
					</div>
				</div>
				
				<!-- Form itself -->
				<form name="sentMessage" id="applicationForm" class="form-horizontal" action="complete.php" method="post">
					<section class="panel">
						<header class="panel-heading form-heading">パスワード設定</header>
						<div class="panel-body form-body" id="js_showPass">
							
							<div class="form-group">
								<label class="col-sm-3 control-label">パスワード</label>
								<div class="col-sm-6">
									<input type="password" name="password" autocomplete="off" value="" class="form-control input new_password">
								</div>
								<div class="col-sm-3">
									<a href="#">表示する</a>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">確認パスワード</label>
								<div class="col-sm-6">
									<input type="password" name="password_again" autocomplete="off" value="" class="form-control input">
								</div>
								<div class="col-sm-3">
									<a href="#">表示する</a>
								</div>
							</div>
							
						</div>
					</section>
					<div class="text-center submit-area">
						<button type="button" class="btn btn-default btn-lg pull-left" onclick="history.back();">戻る</button>
						<button type="submit" class="btn btn-primary btn-lg">パスワードを設定する</button>
					</div>
				</form>
				
			</div>
		</div>
		
	</section>
	
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>


<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Vendor Scripts -->
<script src="js/modernizr.custom.js"></script>
<script src="js/application.js"></script>

<script>
$(function(){
	
	appForm.showPassword('#js_showPass');
	
});

</script>


</body>
</html>