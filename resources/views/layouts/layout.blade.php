<html>
<head>
	<title>@yield('title')</title>
	<meta charset="utf-8">
	<?php //require_once('head.php'); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="robots" content="noindex , nofollow">
	<meta name="format-detection" content="telephone=no">
	<meta name="author" content="http://webthemez.com" />
	<!-- css -->
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
	<link href="css/contents.css" rel="stylesheet" />
</head>
<body>
<div id="wrapper">
	@include('/form/header')
	@yield('inner-headline')
	@yield('form-content')
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- Vendor Scripts -->
	<script src="js/modernizr.custom.js"></script>
	<script src="js/jquery.jpostal.js"></script>
	<script src="js/application.js"></script>

	<script>
	$(function(){
		
		// 郵便番号住所検索実行
		new appForm.insertAddress({
			zip_elm1    : "#user_zip1",
			zip_elm2    : "#user_zip2",
			pref        : "#pref",
			pref_kana   : "#pref_kana",
			city        : "#city",
			city_kana   : "#city_kana",
			address     : "#address1",
			address_kana: "#address1_kana"
		});
		
		
	});
	</script>


</div>

</body>

</html>

 
