<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function pref(){
        $prefectures = [
            '北海道',
             '青森県',
             '岩手県',
             '宮城県',
             '秋田県',
             '山形県',
             '福島県',
             '茨城県',
             '栃木県',
             '群馬県',
             '埼玉県',
             '千葉県',
             '東京都',
             '神奈川県',
             '新潟県',
             '富山県',
             '石川県',
             '福井県',
             '山梨県',
             '長野県',
             '岐阜県',
             '静岡県',
             '愛知県',
             '三重県',
             '滋賀県',
             '京都府',
             '大阪府',
             '兵庫県',
             '奈良県',
             '和歌山県',
             '鳥取県',
             '島根県',
             '岡山県',
             '広島県',
             '山口県',
             '徳島県',
             '香川県',
             '愛媛県',
             '高知県',
             '福岡県',
             '佐賀県',
             '長崎県',
             '熊本県',
             '大分県',
             '宮崎県',
             '鹿児島県',
             '沖縄県'
        ];
        return view('/form/index', compact('prefectures'));
    }
    // public function form_validate(Request $request){
    //     $validate_rule = [
    //         'name_sei' => 'required',
    //         'name_mei' => 'required',
    //         'name_sei_kana' => 'required',
    //         'name_mei_kana' => 'required',
    //         'birthday_y' => 'required',
    //         'birthday_m' => 'required',
    //         'birthday_d' => 'required',
    //         'tel1' => 'required',
    //         'tel2' => 'required',
    //         'tel3' => 'required',
    //         'email' => 'required',
    //         'zip1' => 'required',
    //         'zip2' => 'required',
    //         'adr1' => 'required',
    //         'adr2' => 'required',
    //         'adr3' => 'required',
    //         'adr4' => 'required'
    //     ];
    //     $this->validate($request, $validate_rule);
    //     return view('/form/index');
    // }
    public function confirmation(Request $data){
            $name_sei = $required -> name_sei;
            $name_mei = $required -> name_mei;
            $name_sei_kana = $required -> name_sei_kana;
            $name_mei_kana = $required -> name_mei_kana;
            $birthday_y = $required -> birthday_y;
            $birthday_m = $required -> birthday_m;
            $birthday_d = $required -> birthday_d;
            $tel1 = $required -> tel1;
            $tel2 = $required -> tel2;
            $tel3 = $required -> tel3;
            $email = $required -> email;
            $zip1 = $required -> zip1;
            $zip2 = $required -> zip2;
            $adr1 = $required -> adr1;
            $adr2 = $required -> adr2;
            $adr3 = $required -> adr3;
            $adr4 = $required -> adr4;

            $form_data = $request = [
                'name_sei' => $name_sei,
                'name_mei' => $name_mei,
                'name_sei_kana' => $name_sei_kana,
                'name_mei_kana' => $name_mei_kana,
                'birthday_y' => $birthday_y,
                'birthday_m' => $birthday_m,
                'birthday_d' => $birthday_d,
                'tel1' => $tel1,
                'tel2' => $tel2,
                'tel3' => $tel3,
                'email' => $email,
                'zip1' => $zip1,
                'zip2' => $zip2,
                'adr1' => $adr1,
                'adr2' => $adr2,
                'adr3' => $adr3,
                'adr4' => $adr4
            ];
        return view('/form/conf', compact('form_data'));
    }
}
